# d-to-k
# converts d-values into k-values by taking into account the flow rate

library("openxlsx")
library("dplyr")
library("ggplot2")
library("readxl")
library("data.table")
library("xlsx")

# Save wd
wd <- getwd()

# Read in excel-file
results <- readxl::read_excel(
  "Results.xlsx",
  sheet = "Results",
  col_names = T,
  skip = 3,
  na = c("", "-")
)

# Save column names, but without the ones that are not parameters
# Currently no way of automatically detecting relevant parameters
parameters <- colnames(results)[-(1:10)]

# Loop to convert columns to numeric
for (i in parameters) {
  results[, i] <- as.numeric(unlist(results[, i]))
}

# Remove DoE 16
results <- results %>% filter(`DoE Nr.`!= 16)



# Blank value handling

# Blank value selection
blanks <- results %>% filter(Sample==8)

# Negative values will be ignored
blanks[blanks<0] <- NA

# Calculates arithmetic mean by column
blank_means <- colMeans(blanks[,16:ncol(blanks)], na.rm = TRUE)

# NaN-Value handling, substitution with NA
blank_means[is.nan(blank_means)] <- NA

# Data clean up
results <- results %>% filter(between(t_in_min,0,180))

# Substraction of blank values
for(o in 1:nrow(results)) {
  for (i in 16:length(parameters)) {
    results[o, parameters[i]] <-
      results[o, parameters[i]] - as.numeric(blank_means[parameters[i]])
  }
}



# Deal with negative values in results, substitution with 0
results[results<0] <- 0

# Data clean up
results <- results %>% filter(between(t_in_min,0,180))

# Stores all unique values in Date column 
dates <- results %>% select(Date) %>% pull(Date) %>% unique()  
dates <- dates[!is.na(dates)]

# Stores all unique values in Doe column 
does <- results %>% select(`DoE Nr.`) %>% pull(`DoE Nr.`) %>% unique()  
does <- does[!is.na(does)]

results <- results |> filter(Wavelength != "without")

# Dataframe with cutoff values

cutoff <- readxl::read_excel(
  "cutoff_values.xlsx",
  sheet = "Tabelle1",
  col_names = T,
  na = c("", "-")
)


# One date, one parameter
# Creates Function to generate k values, n of values, and determination
# _spec = specific
min_reduc_generate <- function(data, doe, parameter_spec) {
  
  # Takes only specified dates
  data_filtered <- data %>% filter(`DoE Nr.`==doe)
  
  # Takes the concentrations of one substance and saves it to a substance-vec
  substance_vec <-
    data_filtered %>% select(parameter_spec) %>% pull(parameter_spec)
  
  # Introduces 10% rule
  substance_vec_k <- substance_vec[substance_vec>=substance_vec[1]/10]
  
  # Calculates the difference between logarithmic values
  ln_calc <- c(1:7)
  for (i in 1:length(ln_calc)) {
    ln_calc[i] <- log(substance_vec_k[1]) - log(substance_vec_k[i])
  }
  
  # Unlists ln_calc
  ln_calc <- unlist(ln_calc)
  
  # Deals with Inf Values
  ln_calc <- ln_calc[!is.infinite(ln_calc)]
  

  
  # Time correction

  # Takes irradiation vector of first experiment
  irradiation_vec_first <-
    results %>% filter(Date == dates[1]) %>% filter(between(t_in_min, 10, 180)) %>% select(Irradiation) %>% pull(Irradiation)
  
  irrad_mean_first <- mean(irradiation_vec_first)
  
  # Takes irradiation vector of specified experiment
  irradiation_vec_spec <-
    data_filtered %>% filter(between(t_in_min, 10, 180)) %>% select(Irradiation) %>% pull(Irradiation)
  
  irrad_mean_spec <- mean(irradiation_vec_spec)
  
  # If mean is NA, then mean of first experiment is taken, so that correction factor is 1 
  irrad_mean_spec[is.na(irrad_mean_spec)] <- irrad_mean_first
  
  
  
  # Loss of Irradiation in percent of specified experiment
  irrad_loss <- 1- irrad_mean_spec/irrad_mean_first
  
  # Factor of quartz sleeve without sensor
  quartz_factor <- 1-(66.2-40.5)/(112-40.5)
  
  # Loss of Irradiation due to quartz sleeve
  irrad_loss_quartz <- irrad_loss*quartz_factor
  
  # Factor for time correction, percent
  irrad_correction <- 1 - irrad_loss_quartz
  
  # Extracts time values
  times <- data_filtered %>% select(t_in_min)
  times <- times %>% pull(t_in_min)
  times <- times*irrad_correction
  
  # k-value calculation
  # ln_calc is adjusted to number of values specified in a table
  
  ln_calc <- ln_calc[1:unname(unlist(cutoff[,parameter_spec]))]
  
  # Generates linear regression model
  model <- lm(ln_calc ~ times[1:length(ln_calc)])
  
  # Extracts k value from model
  # k_value in 1/min
  k_value <- coef(model)[2]
  # k_value in 1/s
  k_value_s <- k_value / 60
  
  # Creates table with information on experiment
  doe <- data_filtered %>% select(`DoE Nr.`) %>% pull(`DoE Nr.`)
  flow <- data_filtered %>% select(`Flow rate`) %>% pull(`Flow rate`)
  wave <- data_filtered %>% select(Wavelength) %>% pull(Wavelength)
  water <- data_filtered %>% select(Water) %>% pull(Water)
  perox <- data_filtered %>% select(Peroxide) %>% pull(Peroxide)
  
  # Determine value for flow rate and dwelling time in seconds
  # Dwelling times might change when geometry of reactor is updated
  # Current volume: 400 mL
  
  if(flow[1]=="high"){
    fr <- 100
    dwell <- 4
  }else if(flow[1]=="medium"){
    fr <- 76
    dwell <- 5.26
  }else if(flow[1]=="low"){
    fr <- 40
    dwell <- 10
  }
  

  # Regression model for concentration, k in 1/s
  # k-value in reality is a d-value
  c <- function(x){
    c = substance_vec[1]*exp(-k_value_s*x)
    return(c)
  }
  
  # Calculates concentration at dwelling time in the mixer
  c_m <- c(dwell)
  
  # Volume of mixer in mL
  vol_mix <- 10000
  
  # Multiplies it with factor
  c_uv <- c_m*(1-(vol_mix/fr)*(k_value_s))
  
  reduc <- 1-c_uv/substance_vec[1]
  
  k_real <- log(c_m/c_uv)/dwell
  
  
  
  # Creates dataframe to return and returns it
  output <- data.frame(
    Doe = doe[1],
    Flow_rate = flow[1],
    Wavelength = wave[1],
    water = water[1],
    Peroxide = as.character(perox[1]),
    c_m = c_m,
    Reduction = reduc,
    #already changed k to d
    d_value_s = k_value_s,
    k_value_real = k_real,
    # energy = joule,
    row.names = TRUE
  )
  
  return(output)
  
}

# All dates, one parameter
# Creates the starting dataframe
# Parameter_spec variable: currently no way of detecting correct number

starting_param <-  min_reduc_generate(data = results,
                                      doe = 1,
                                      parameter_spec = parameters[17])
for (i in 2:14) {
  starting_param <-
    rbindlist(list(
      starting_param,
      min_reduc_generate(data = results, 
                         doe = i, 
                         parameters[17])
    ))
}

# Prints starting dataframe into first sheet of new excel file
xlsx::write.xlsx2(
  starting_param,
  "d-to-k.xlsx",
  row.names = FALSE,
  sheetName = parameters[17] # Currently no way of detecting correct number
)


# # Function that appends sheets to existing excel file
# # Does the same thing as the code lines above, just within a function
# # Calls min_reduc_generate function from within
printframe <- function(data, does_vec, parameter_spec) {
  
  # One date, one parameter
  printframe_param <-
    min_reduc_generate(data = data, 
                       doe = 1, 
                       parameter_spec)
  
  # All dates, one parameter
  for (i in 2:length(does_vec)) {
    printframe_param <-
      rbindlist(list(
        printframe_param,
        min_reduc_generate(data = data, 
                           doe= i, 
                           parameter_spec =parameter_spec)
      ))
  }
  
  # Appends it to existing excel sheet
  xlsx::write.xlsx2(
    printframe_param,
    "d-to-k.xlsx",
    row.names = FALSE,
    sheetName = parameter_spec,
    append = TRUE
  )
}



# # Loop that goes through the parameters
# # Currently no way of detecting correct parameter numbers
for(i in 18:length(parameters)) {
  printframe(data = results,
             does_vec = c(1:14),
             parameter_spec = parameters[i])
}